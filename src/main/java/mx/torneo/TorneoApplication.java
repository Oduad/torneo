package mx.torneo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TorneoApplication {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(TorneoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(TorneoApplication.class, args);
		LOGGER.info("Application started");
	}
}
