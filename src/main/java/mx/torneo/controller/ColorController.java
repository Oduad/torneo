package mx.torneo.controller;

import java.util.List;

import mx.torneo.dto.ColorDTO;

public interface ColorController {

	List<ColorDTO>getAllColors();
	
}
