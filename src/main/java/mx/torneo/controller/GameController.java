package mx.torneo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.torneo.dto.TeamDTO;
import mx.torneo.entity.Game;
import mx.torneo.entity.Team;
import mx.torneo.dto.PlayerDTO;
import mx.torneo.dto.GameDTO;

@RestController
@RequestMapping(path = "game")
public interface GameController {

	List<GameDTO> getAllGames(); // ¿Por qué no te sugiere un getAll o dónde debe sugerirlo?

	GameDTO fillResult(final Long id, GameDTO gameDTO);
	
	List<GameDTO> generateAllGames();

	GameDTO setHour(final GameDTO gameDTO);

}
