package mx.torneo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.torneo.dto.PlayerDTO;

@RestController
@RequestMapping(path = "jugador")
public interface PlayerController {

    List<PlayerDTO> getAllPlayers();

    PlayerDTO getPlayerById(Long id);
    
    PlayerDTO savePlayer(final PlayerDTO playerDTO);                 //This is for creation
    
    PlayerDTO updatePlayer(final Long id, final PlayerDTO playerDTO);     //This is for update
    
}
