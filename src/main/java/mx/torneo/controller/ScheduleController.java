package mx.torneo.controller;

import mx.torneo.dto.ScheduleDTO;

public interface ScheduleController {

	ScheduleDTO SaveSchedule();
	
}
