package mx.torneo.controller;

import java.util.List;

import mx.torneo.dto.PlayerDTO;
import mx.torneo.dto.TeamDTO;

public interface TeamController {

    List<TeamDTO> getAllTeams();

    TeamDTO saveTeam(TeamDTO teamoDTO);         //This is for saving a new team
    
    TeamDTO saveTeam(Long id, TeamDTO teamoDTO); //This is for update a team
    
    //List<TeamDTO> get2Highest(TeamDTO arrayListInt);
    
    TeamDTO findByName(String name);

    TeamDTO getTeamById(Long id);

	List<TeamDTO> get2Highest(TeamDTO arrayListInt);

	List<TeamDTO> get2GeneralTable();

}
