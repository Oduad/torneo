package mx.torneo.controller.error;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import mx.torneo.dto.error.RestMessage;
import mx.torneo.exception.DuplicateTeamException;
import mx.torneo.exception.IncompleteTeamsException;
import mx.torneo.exception.InvalidDateException;
import mx.torneo.exception.InvalidNumberException;
import mx.torneo.exception.StringNullException;
import mx.torneo.exception.NotFoundException;
import mx.torneo.exception.TeamIsFullyException;

// Lo que hace esta clase es que cada que haya un error en saveTeam, 
// se rompe el flujo y llegue en automático a esta clase. Es para 
// controlar TODOS los errores en un solo lugar.

@ControllerAdvice
public class RestExceptionHandler {

	private final MessageSource messageSource;

	@Autowired // Este es para la inyección(messageSource) ya se hizo en algún lado y se asigna
				// a este atributo)
	public RestExceptionHandler(final MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@ExceptionHandler(TeamIsFullyException.class)
	public ResponseEntity<RestMessage> handleIllegalArgument(final TeamIsFullyException ex, final Locale locale) {
		String errorMessage = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale);
		return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(DuplicateTeamException.class)
	public ResponseEntity<RestMessage> handleIllegalArgument(final DuplicateTeamException ex, final Locale locale) {
		String errorMessage = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale);
		return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.INTERNAL_SERVER_ERROR);// Aquí no es ISE
	}
	
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<RestMessage> handleIllegalArgument(final NotFoundException ex, final Locale locale) {
		String errorMessage = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale);
		return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST);// Aquí no es
	}
	
	@ExceptionHandler(IncompleteTeamsException.class)
	public ResponseEntity<RestMessage> handleIllegalArgument(final IncompleteTeamsException ex, final Locale locale) {
		String errorMessage = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale);
		return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST);// Aquí no es
	}
	
	@ExceptionHandler(InvalidDateException.class)
	public ResponseEntity<RestMessage> handleIllegalArgument(final InvalidDateException ex, final Locale locale) {
		String errorMessage = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale);
		return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST);// Aquí no es
	}
	
	@ExceptionHandler(StringNullException.class)
	public ResponseEntity<RestMessage> handleIllegalArgument(final StringNullException ex, final Locale locale) {
		String errorMessage = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale);
		return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST);// Aquí no es
	}
	
	@ExceptionHandler(InvalidNumberException.class)
	public ResponseEntity<RestMessage> handleIllegalArgument(final InvalidNumberException ex, final Locale locale) {
		String errorMessage = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale);
		return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST);// Aquí no es
	}
	
	/*@ExceptionHandler(LastNameNullException.class)
	public ResponseEntity<RestMessage> handleIllegalArgument(final LastNameNullException ex, final Locale locale) {
		String errorMessage = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale);
		return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST);// Aquí no es
	}
	
	@ExceptionHandler(MiddleNameNullException.class)
	public ResponseEntity<RestMessage> handleIllegalArgument(final MiddleNameNullException ex, final Locale locale) {
		String errorMessage = messageSource.getMessage(ex.getMessage(), ex.getArgs(), locale);
		return new ResponseEntity<>(new RestMessage(errorMessage), HttpStatus.BAD_REQUEST);// Aquí no es
	}*/
}
