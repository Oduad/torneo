package mx.torneo.controllerImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.torneo.controller.ColorController;
import mx.torneo.dto.ColorDTO;
import mx.torneo.dto.PlayerDTO;
import mx.torneo.entity.Color;
import mx.torneo.service.ColorService;

@RestController
@RequestMapping(path = "color")
public class ColorControllerImp implements ColorController {

	private ColorService colorService;

	@Autowired
	public ColorControllerImp(final ColorService colorService) {
		this.colorService = colorService;
	}

	@GetMapping(path = "")
	@Override
	public List<ColorDTO> getAllColors() {
		return colorService.getAllColors();
	}

}
