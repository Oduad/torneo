package mx.torneo.controllerImp;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.torneo.controller.GameController;
import mx.torneo.dto.TeamDTO;
import mx.torneo.entity.Game;
import mx.torneo.entity.Team;
import mx.torneo.dto.PlayerDTO;
import mx.torneo.dto.GameDTO;
import mx.torneo.serviceImp.GameServiceImp;
import mx.torneo.serviceImp.TeamServiceImp;

@RestController
@RequestMapping(path = "game")
public class GameControllerImp implements GameController {

	private GameServiceImp gameService;

	@Autowired
	public GameControllerImp(final GameServiceImp gameService) {
		this.gameService = gameService;
	}

	@Override
	@PostMapping(path = "")
	public List<GameDTO> generateAllGames() {
		List<GameDTO> teamsResponse = gameService.generateAllGames();
		return teamsResponse;
	}

	@Override
	@PutMapping(path = "/fill/{id}")
	public GameDTO fillResult(final @PathVariable  Long id,final @RequestBody GameDTO gameDTO) {
		GameDTO gameDTOResponse = gameService.fillResult(id,gameDTO);
		return gameDTOResponse;
	}

	@Override
	@GetMapping(path = "/getAllGames")
	public List<GameDTO> getAllGames() {
		List<GameDTO> teamsResponse = gameService.getAllGames();
		return teamsResponse;
	}

	@Override
	public GameDTO setHour(GameDTO gameDTO) {
		// TODO Auto-generated method stub
		return null;
	}
}
