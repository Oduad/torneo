package mx.torneo.controllerImp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.torneo.controller.PlayerController;
import mx.torneo.dto.PlayerDTO;
import mx.torneo.dto.TeamDTO;
import mx.torneo.serviceImp.PlayerServiceImp;
import mx.torneo.serviceImp.ValidateServiceImp;

@RestController
@RequestMapping(path = "player")
public class PlayerControllerImp implements PlayerController {

	private PlayerServiceImp playerService;
	private ValidateServiceImp validateServieImp;

	@Autowired
	public PlayerControllerImp(final PlayerServiceImp playerService, final ValidateServiceImp validateServieImp) {
		this.playerService = playerService;
		this.validateServieImp = validateServieImp;
	}

	@Override
	@GetMapping(path = "")
	public List<PlayerDTO> getAllPlayers() {
		List<PlayerDTO> playersResponse = playerService.getAllPlayers();
		return playersResponse;
	}

	@Override
	@GetMapping(path = "{id}")
	public PlayerDTO getPlayerById(final @PathVariable Long id) {
		return playerService.getPlayerById(id);
	}

	@Override // This is for creation
	@PostMapping(path = "/save")
	public PlayerDTO savePlayer(final @RequestBody PlayerDTO playerDTO) {
		/*
		 * validateServieImp.validatePlayerName(playerDTO.getName(),
		 * playerDTO.getLastName(), playerDTO.getMiddleName());
		 * validateServieImp.validatePlayerLastName(playerDTO.getName());
		 * validateServieImp.validateMiddleName(playerDTO.getName());
		 */
		validateServieImp.validateX(playerDTO.getName(), "name.null");
		validateServieImp.validateX(playerDTO.getLastName(), "lastname.null");
		validateServieImp.validateX(playerDTO.getMiddleName(), "middlename.null");
		//validateServieImp.validateX(playerDTO.getMiddleName(), "middlename.null");
		validateServieImp.validateNumber(playerDTO.getNumber(), "number.negative");
		validateServieImp.validatePlayerDate(playerDTO.getBirthDate(), "team.dateInv");
		return playerService.savePlayer(Optional.empty(),playerDTO);
	}

	@Override // This is for update   
	@PutMapping(path = "{id}")
	public PlayerDTO updatePlayer(final @PathVariable Long id, final @RequestBody PlayerDTO playerDTO) {
		return playerService.savePlayer(Optional.of(id),playerDTO);
	}
}


