package mx.torneo.controllerImp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import mx.torneo.controller.TeamController;
import mx.torneo.dto.PlayerDTO;
import mx.torneo.dto.TeamDTO;
import mx.torneo.serviceImp.PlayerServiceImp;
import mx.torneo.serviceImp.TeamServiceImp;
import mx.torneo.serviceImp.ValidateServiceImp;

@RestController
@RequestMapping(path = "team")
public class TeamControllerImp implements TeamController {

    private TeamServiceImp teamService;
    private ValidateServiceImp validateServieImp;
    
    @Autowired
    public TeamControllerImp(final TeamServiceImp teamService, final ValidateServiceImp validateServieImp) {
        this.teamService = teamService;
        this.validateServieImp = validateServieImp;
    }

    @Override
    @PutMapping(path = "/{id}")
    public TeamDTO saveTeam(final @PathVariable Long id, final @RequestBody TeamDTO teamDTO) {
    	//validateServieImp.validateTeam(teamDTO);
    	validateServieImp.validateX(teamDTO.getName(), "teamname.null");
        return teamService.saveTeam(Optional.of(id), teamDTO);
    }

    @Override
    @PostMapping(path = "")
    public TeamDTO saveTeam(final @RequestBody TeamDTO teamDTO) { 
    	validateServieImp.validateTeam(teamDTO);
        return teamService.saveTeam(Optional.empty(), teamDTO);
    }

    @Override
    @GetMapping(path = "")
    public List<TeamDTO> getAllTeams() {
        List<TeamDTO> teamResponse = teamService.getAllTeams();
        return teamResponse;
    }

    @Override
    @GetMapping(path = "{id}")
    public TeamDTO getTeamById(final @PathVariable Long id) {
        return teamService.getTeamById(id);
    }

    @Override
    @GetMapping(path = "/highest")
    public List<TeamDTO> get2Highest(final TeamDTO arrayListInt) {
    	List<TeamDTO> teamResponse = teamService.get2Highest();
        return teamResponse;
    }

    @Override
    @GetMapping(path = "/name/{name}")
    public TeamDTO findByName(final @PathVariable String name) {
    	 return teamService.findByName(name);
    }
    
    @Override
    @GetMapping(path = "/general")
    public List<TeamDTO> get2GeneralTable() {
    	List<TeamDTO> teamResponse = teamService.get2GeneralTable();
        return teamResponse;
    }
}

//final @PathVariable String date

