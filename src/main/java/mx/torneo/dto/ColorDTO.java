package mx.torneo.dto;

public class ColorDTO {

	private Long id;
	private String nameColor;
	
	public ColorDTO(Long id, String nameColor) {
		super();
		this.id = id;
		this.nameColor = nameColor;
	}

	public ColorDTO() {
		
	}

	public Long getId() {
		return id;
	}

	public String getNameColor() {
		return nameColor;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNameColor(String nameColor) {
		this.nameColor = nameColor;
	}
}
