package mx.torneo.dto;

import java.util.Date;

public class GameDTO {

	private Long id;
	private Long idLocal;
	private Long idVisitor;
	private String date;
	private String localTeam;
	private String visitorTeam;
	private int localPoints;
	private int visitorPoints;
	private String time;
	
	public GameDTO(Long id, Long idLocal, Long idVisitor, String date, String localTeam, String visitorTeam, 
			int localPoints, int visitorPoints, String time) {
		super();
		this.id = id;
		this.idLocal = idLocal;
		this.idVisitor = idVisitor;
		this.date = date;
		this.localTeam = localTeam;
		this.visitorTeam = visitorTeam;
		this.localPoints = localPoints;
		this.visitorPoints = visitorPoints;
		this.time=time;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public GameDTO(/*int idLocal, int idVisitor, Date date,*/ String localTeam, String visitorTeam, 
			int localPoints, int visitorPoints, String time) {
		/*super();
		this.idLocal = idLocal;
		this.idVisitor = idVisitor;
		this.date = date;*/
		this.localTeam = localTeam;
		this.visitorTeam = visitorTeam;
		this.localPoints = localPoints;
		this.visitorPoints = visitorPoints;
		this.time = time;
	}
	
	public GameDTO() {
	}
	
	public Long getIdLocal() {
		return idLocal;
	}
	public Long getIdVisitor() {
		return idVisitor;
	}
	public String getDate() {
		return date;
	}
	public String getLocalTeam() {
		return localTeam;
	}
	public String getVisitorTeam() {
		return visitorTeam;
	}
	public int getLocalPoints() {
		return localPoints;
	}
	public int getVisitorPoints() {
		return visitorPoints;
	}
	public void setIdLocal(Long l) {
		this.idLocal = l;
	}
	public void setIdVisitor(Long l) {
		this.idVisitor = l;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setLocalTeam(String localTeam) {
		this.localTeam = localTeam;
	}
	public void setVisitorTeam(String visitorTeam) {
		this.visitorTeam = visitorTeam;
	}
	public void setLocalPoints(int localPoints) {
		this.localPoints = localPoints;
	}
	public void setVisitorPoints(int visitorPoints) {
		this.visitorPoints = visitorPoints;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
}
