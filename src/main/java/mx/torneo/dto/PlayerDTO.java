package mx.torneo.dto;

import java.time.LocalDate;

public class PlayerDTO {
	private Long id;
	private String name;
	private String lastName;
	private String middleName;
	private LocalDate birthDate;
	private int number;
	private TeamDTO team;
	private String curp;

	public PlayerDTO(Long id, String name, String lastName, String middleName, LocalDate birthDate,
			int number, TeamDTO team, String curp) {
		super();
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.middleName = middleName;
		this.birthDate = birthDate;
		this.number = number;
		this.team = team;
		this.curp = curp;
	}
	
	public PlayerDTO() {

	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getLastName() {
		return lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public int getNumber() {
		return number;
	}
	
	public String getCurp() {
		return curp;
	}
	
	public TeamDTO getTeam() {
		return team;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public void setTeam(TeamDTO team) {
		this.team = team;
	}
}
