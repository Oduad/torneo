package mx.torneo.dto;

import java.util.Date;

public class ScheduleDTO {

	private int id;
	
	private Date schedule;

	public int getId() {
		return id;
	}

	public Date getSchedule() {
		return schedule;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setSchedule(Date schedule) {
		this.schedule = schedule;
	}
	
}
