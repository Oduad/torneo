package mx.torneo.dto;

import java.util.ArrayList;
import java.util.List;

import mx.torneo.entity.Color;
import mx.torneo.entity.Player;

public class TeamDTO implements Comparable<TeamDTO>{

	private Long id;
	private String name;
	private Color localColor;
	private Color visitorColor;
	private List<PlayerDTO> players = new ArrayList<PlayerDTO>();
	private Integer points;
	
	public List<PlayerDTO> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayerDTO> players) {
		this.players = players;
	}

	public TeamDTO(Long id, String name, Color localColor, Color visitorColor, List<PlayerDTO> players) {
		super();
		this.id = id;
		this.name = name;
		this.localColor = localColor;
		this.visitorColor = visitorColor;
		this.players=players;
	}
	
	public TeamDTO(Long id, String name, Integer points) {
		super();
		this.id = id;
		this.name = name;
		this.points = points;
	}
	
	public TeamDTO(Integer points) {
		super();
		this.points = points;
	}

	public TeamDTO() {

	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Color getLocalColor() {
		return localColor;
	}

	public Color getVisitorColor() {
		return visitorColor;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setLocalColor(Color localColor) {
		this.localColor = localColor;
	}

	public void setVisitorColor(Color visitorColor) {
		this.visitorColor = visitorColor;
	}

	@Override
    public int compareTo(TeamDTO team) {
		return points.compareTo(team.getPoints());
    }
}
