package mx.torneo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="color")
public class Color {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nameColor;

	public Long getId() {
		return id;
	}

	public String getNameColor() {
		return nameColor;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNameColor(String nameColor) {
		this.nameColor = nameColor;
	}	
}
