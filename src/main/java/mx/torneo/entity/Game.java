package mx.torneo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="game")
public class Game {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long idLocal;
	private Long idVisitor;
	private String date;
	//@Column(length = 15)			//Esto es para que
	private String localTeam;
	//@Column(length = 15)
	private String visitorTeam;
	private int localPoints;
	private int visitorPoints;
	private String time;
	
	public Game(Long id, Long idLocal, Long idVisitor, String date, String localTeam, String visitorTeam, 
			int localPoints, int visitorPoints, String time) {
		super();
		this.id = id;
		this.idLocal = idLocal;
		this.idVisitor = idVisitor;
		this.date = date;
		this.localTeam = localTeam;
		this.visitorTeam = visitorTeam;
		this.localPoints = localPoints;
		this.visitorPoints = visitorPoints;
		this.time = time;
	}
	
	/*public Game(int idLocal, int idVisitor, Date date, String localTeam, String visitorTeam, 
			int localPoints, int visitorPoints) {
		super();
		this.idLocal = idLocal;
		this.idVisitor = idVisitor;
		this.date = date;
		this.localTeam = localTeam;
		this.visitorTeam = visitorTeam;
		this.localPoints = localPoints;
		this.visitorPoints = visitorPoints;
	}*/
	 
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Game() {

	}
	
	public Long getIdLocal() {
		return idLocal;
	}
	public Long getIdVisitor() {
		return idVisitor;
	}
	public String getDate() {
		return date;
	}
	
	/*public long getTime() { Revisar este método
		return date.getTime();
	}*/
	
	public String getTime() {
		return time;
	}
	
	public String getLocalTeam() {
		return localTeam;
	}
	public String getVisitorTeam() {
		return visitorTeam;
	}
	public int getLocalPoints() {
		return localPoints;
	}
	public int getVisitorPoints() {
		return visitorPoints;
	}
	public void setIdLocal(Long idLocal) {
		this.idLocal = idLocal;
	}
	public void setIdVisitor(Long idVisitor) {
		this.idVisitor = idVisitor;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setLocalTeam(String localTeam) {
		this.localTeam = localTeam;
	}
	public void setVisitorTeam(String visitorTeam) {
		this.visitorTeam = visitorTeam;
	}
	public void setLocalPoints(int localPoints) {
		this.localPoints = localPoints;
	}
	public void setVisitorPoints(int visitorPoints) {
		this.visitorPoints = visitorPoints;
	}

	public void setTime(String time) {
		this.time = time;
	}
}
