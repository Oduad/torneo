package mx.torneo.entity;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "player")
public class Player {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(length = 15)
	private String name;
	@Column(length = 15)
	private String lastName;
	@Column(length = 15)
	private String middleName;
	private LocalDate birthDate;
	private int number;
	@Column(length = 18)
	private String curp;
	
	@ManyToOne
	@JoinColumn(name = "id_team", nullable = false, updatable = false)
	private Team team;		//Java generalmente maneja objetos o hace referencia a objetos y lo facilita.

	public Player(Long id, String name, String lastName, String middleName, LocalDate birthDate, int number, Team team,
	String curp) {
		super();
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.middleName = middleName;
		this.birthDate = birthDate;
		this.number = number;
		this.team = team;
		this.curp = curp;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public Player() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	//@ManyToOne
	//@JoinColumn(name = "id_team", nullable = false, updatable = false)
	public Team getTeam() { 
		return team;
	}

	public void setTeam(Team team2) {
		this.team = team2;
	}
}
