package mx.torneo.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="schedule")
public class Schedule {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private Date schedule;

	public int getId() {
		return id;
	}

	public Date getSchedule() {
		return schedule;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setSchedule(Date schedule) {
		this.schedule = schedule;
	}
}
