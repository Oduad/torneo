package mx.torneo.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import mx.torneo.dto.PlayerDTO;

@Entity
@Table(name = "team")
public class Team {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 25)
	private String name;
	
	@ManyToOne // Esto es para la relación uno a muchos.
	@JoinColumn(name = "id_local_color", nullable = false)
	private Color localColor;
	
	@ManyToOne // Esto es para la relación uno a muchos.
	@JoinColumn(name = "id_visitor_color", nullable = false)
	private Color visitorColor;
	
    //@ElementCollection(targetClass=Integer.class)
    //@OneToMany(/*targetEntity = Team.class, */mappedBy = "team", cascade = CascadeType.ALL/*, fetch = FetchType.EAGER*/)
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "team")
	private List<Player> players = new ArrayList<Player>();
	
	private Integer points;

	public Team(Long id, String name, Color localColor, Color visitorColor, List<Player> players) {
		super();
		this.id = id;
		this.name = name;
		this.localColor = localColor;
		this.visitorColor = visitorColor;
		this.players = players;
	}
	
	public Team(Long id, String name, Integer points) {
		super();
		this.id = id;
		this.name = name;
		this.points = points;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public Team() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long l) {
		this.id = l;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Color getLocalColor() {
		return localColor;
	}

	public void setLocalColor(Color localColor) {
		this.localColor = localColor;
	}

	public Color getVisitorColor() {
		return visitorColor;
	}

	public void setVisitorColor(Color visitorColor) {
		this.visitorColor = visitorColor;
	}

}
