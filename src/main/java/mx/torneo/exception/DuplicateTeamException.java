package mx.torneo.exception;

public class DuplicateTeamException extends RuntimeException {

	private Object[] args;

	private String message;

	public Object[] getArgs() {
		return args;
	}

	public DuplicateTeamException(String message, Object[] args) {
		super();
		this.args = args;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
