package mx.torneo.exception;

public class GameAlreadyException extends RuntimeException{

    private static final long serialVersionUID = 8513511889801716463L;

    private String message;
    private Object[] args;

    public GameAlreadyException(final String message, final Object[] args) {
        this.args = args;
        this.message = message;
    }
    
    @Override
    public String getMessage() {
        return message;
    }

    public Object[] getArgs() {
        return args;
    }
    
}
