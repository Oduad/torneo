package mx.torneo.exception;

public class IncompleteTeamException extends RuntimeException {

	private Object[] args;

	private String message;

	public Object[] getArgs() {
		return args;
	}

	public IncompleteTeamException(String message, Object[] args) {
		super();
		this.args = args;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
