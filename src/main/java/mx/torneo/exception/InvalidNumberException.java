package mx.torneo.exception;

public class InvalidNumberException extends RuntimeException {

	private Object[] args;

	private String number;

	public Object[] getArgs() {
		return args;
	}

	public InvalidNumberException(String message, Object[] args) {
		super();
		this.args = args;
		this.number = message;
	}

	public String getMessage() {
		return number;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}

	public void setMessage(String message) {
		this.number = message;
	}
}
