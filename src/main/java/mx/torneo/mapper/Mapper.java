package mx.torneo.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;
import mx.torneo.dto.TeamDTO;
import mx.torneo.dto.PlayerDTO;
import mx.torneo.dto.ColorDTO;
import mx.torneo.dto.GameDTO;
import mx.torneo.entity.Team;
import mx.torneo.serviceImp.TeamServiceImp;
import mx.torneo.entity.Player;
import mx.torneo.entity.Color;
import mx.torneo.entity.Game;

@Component // This makes possible the injection
public class Mapper {

	public TeamDTO team2teamDTO(final Team team) {
		TeamDTO teamDTO = new TeamDTO();
		List<PlayerDTO> playersListDTO = new ArrayList<PlayerDTO>();

		teamDTO.setId(team.getId());
		teamDTO.setName(team.getName());
		teamDTO.setLocalColor(team.getLocalColor());
		teamDTO.setVisitorColor(team.getVisitorColor());
		//teamDTO.setPoints(team.getPoints());
		// Un Team tiene un array de jugadores pero son Entities y hay que pasarlos a
		// DTO
		for (int i = 0; i < team.getPlayers().size(); i++) {
			PlayerDTO playerDTO = player2playerDTO(team.getPlayers().get(i));
			playersListDTO.add(playerDTO);
		}
		System.out.println("El equipo " + team.getName() + " tiene " + team.getPlayers().size() + " elementos.");
		teamDTO.setPlayers(playersListDTO);
		return teamDTO;
	}

	/*public TeamDTO team2teamDTOCreation(final Team team) {
		TeamDTO teamDTO = new TeamDTO();
		List<PlayerDTO> playersListDTO = new ArrayList<PlayerDTO>();

		teamDTO.setId(team.getId());
		teamDTO.setName(team.getName());
		teamDTO.setLocalColor(team.getLocalColor());
		teamDTO.setVisitorColor(team.getVisitorColor());
		for (int i = 0; i < team.getPlayers().size(); i++) {
			PlayerDTO playerDTO = player2playerDTO(team.getPlayers().get(i));
			playersListDTO.add(playerDTO);
		}
		System.out.println("El equipo " + team.getName() + " tiene " + team.getPlayers().size() + " elementos.");
		teamDTO.setPlayers(playersListDTO);
		return teamDTO;
	}*/

	public TeamDTO team2teamDTOforTable(final Team team) {
		TeamDTO teamDTO = new TeamDTO();
		teamDTO.setId(team.getId());
		teamDTO.setName(team.getName());
		teamDTO.setPoints(team.getPoints());
		return teamDTO;
	}

	/*public Team teamDTO2teamCreation(final TeamDTO teamDTO) {
		Team team = new Team();
		List<Player> playersList = new ArrayList<Player>();
		team.setId(teamDTO.getId());
		team.setName(teamDTO.getName());
		team.setLocalColor(teamDTO.getLocalColor());
		team.setVisitorColor(teamDTO.getVisitorColor());
		for (int i = 0; i < teamDTO.getPlayers().size(); i++) {
			Player player = playerDTO2player(teamDTO.getPlayers().get(i));
			playersList.add(player);
		}
		team.setPlayers(playersList);
		return team;
	}*/

	public Team teamDTO2team(final TeamDTO teamDTO) {
		Team team = new Team();
		List<Player> playersList = new ArrayList<Player>();
		team.setId(teamDTO.getId());
		team.setName(teamDTO.getName());
		team.setLocalColor(teamDTO.getLocalColor());
		team.setVisitorColor(teamDTO.getVisitorColor());
		//team.setPoints(teamDTO.getPoints());
		for (int i = 0; i < teamDTO.getPlayers().size(); i++) {
			Player player = playerDTO2player(teamDTO.getPlayers().get(i));
			playersList.add(player);
		}
		team.setPlayers(playersList);
		return team;
	}

	public PlayerDTO player2playerDTO(final Player player) {
		// TeamDTO teamDTO = team2teamDTO(player.getTeam());
		PlayerDTO playerDTO = new PlayerDTO();
		playerDTO.setId(player.getId());
		playerDTO.setName(player.getName());
		playerDTO.setLastName(player.getLastName());
		playerDTO.setMiddleName(player.getMiddleName());
		playerDTO.setBirthDate(player.getBirthDate());
		playerDTO.setNumber(player.getNumber());
		playerDTO.setCurp(player.getCurp());
		// playerDTO.setTeam(teamDTO);
		return playerDTO;
	}

	public Player playerDTO2player(final PlayerDTO playerDTO) {
		Team team = teamDTO2team(playerDTO.getTeam());
		Player player = new Player();
		player.setName(playerDTO.getName());
		player.setLastName(playerDTO.getLastName());
		player.setMiddleName(playerDTO.getMiddleName());
		player.setBirthDate(playerDTO.getBirthDate());
		player.setNumber(playerDTO.getNumber());
		player.setId(playerDTO.getId());
		player.setTeam(team);
		player.setCurp(playerDTO.getCurp());
		return player;
	}

	public ColorDTO color2colorDTO(final Color color) {
		ColorDTO colorDTO = new ColorDTO();
		colorDTO.setId(color.getId());
		colorDTO.setNameColor(color.getNameColor());
		return colorDTO;
	}

	public Color colorDTO2color(final ColorDTO colorDTO) {
		Color color = new Color();
		color.setId(colorDTO.getId());
		color.setNameColor(colorDTO.getNameColor());
		return color;
	}

	public GameDTO game2gameDTO(final Game game) {
		GameDTO gameDTO = new GameDTO();
		gameDTO.setId(game.getId());
		gameDTO.setIdLocal(game.getIdLocal());
		gameDTO.setIdVisitor(game.getIdVisitor());
		gameDTO.setDate(game.getDate());
		gameDTO.setLocalTeam(game.getLocalTeam());
		gameDTO.setVisitorTeam(game.getVisitorTeam());
		gameDTO.setLocalPoints(game.getLocalPoints());
		gameDTO.setVisitorPoints(game.getVisitorPoints());
		gameDTO.setTime(game.getTime());
		return gameDTO;
	}

	public Game gameDTO2game(final GameDTO gameDTO) {
		Game game = new Game();
		game.setId(gameDTO.getId());
		game.setIdLocal(gameDTO.getIdLocal());
		game.setIdVisitor(gameDTO.getIdVisitor());
		game.setDate(gameDTO.getDate());
		game.setLocalTeam(gameDTO.getLocalTeam());
		game.setVisitorTeam(gameDTO.getVisitorTeam());
		game.setLocalPoints(gameDTO.getLocalPoints());
		game.setVisitorPoints(gameDTO.getVisitorPoints());
		game.setTime(gameDTO.getTime());
		return game;
	}
}
