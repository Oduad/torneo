package mx.torneo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.torneo.entity.Game;

@Repository
public interface GameRepository extends JpaRepository <Game,Long>{

	List<Game> findByDate(String date);

}
