 package mx.torneo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.torneo.entity.Player;

@Repository
public interface PlayerRepository extends JpaRepository <Player,Long>{

}
