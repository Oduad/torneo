package mx.torneo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.torneo.entity.Schedule;


public interface ScheduleRepository extends JpaRepository<Schedule,Long> {

}
