package mx.torneo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.torneo.dto.TeamDTO;
import mx.torneo.entity.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team,Long>{
	
	//Se le ponen 2 parámetros
	//1. La entidad Team
	//2. El tipo de dato del id de esa entidad

	/* Integer va de la mano con el int de la BD y con el int de Equipo
  	   a las llaves primarias las está tratando como un Integer */
	
	List<Team> findAllByName(String name);

	List<Team> findByName(String name);
	
}
