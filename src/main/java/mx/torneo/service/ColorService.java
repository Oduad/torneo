package mx.torneo.service;

import java.util.List;

import mx.torneo.dto.ColorDTO;

public interface ColorService {

	List<ColorDTO> getAllColors();
	
}
