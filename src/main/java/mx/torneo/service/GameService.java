package mx.torneo.service;

import java.util.List;
import java.util.Optional;
import mx.torneo.dto.PlayerDTO;
import mx.torneo.dto.GameDTO;

public interface GameService {
    
    List<GameDTO> generateAllGames();       //¿Por qué no te sugiere un getAll o dónde debe sugerirlo?   

	GameDTO fillResult(Long id,GameDTO gameDTO);
	
	List<GameDTO> getAllGames();
	
	boolean validateAllTeams();
	
}
