package mx.torneo.service;

import java.util.List;
import java.util.Optional;
import mx.torneo.dto.PlayerDTO;
import mx.torneo.dto.TeamDTO;

public interface PlayerService {
	
	List<PlayerDTO> getAllPlayers();       //¿Por qué no te sugiere un getAll o dónde debe sugerirlo?     

    PlayerDTO getPlayerById(Long id);
    
    List<PlayerDTO> getPlayersByTeam(final Optional<Long> idTeam);

	PlayerDTO savePlayer(final Optional<Long> idTeam, final PlayerDTO playerDTO);

	TeamDTO getTeamById(Long idTeam);
	
}
