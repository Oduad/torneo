package mx.torneo.service;

import java.util.List;
import java.util.Optional;
import mx.torneo.dto.TeamDTO;
import mx.torneo.entity.Player;
import mx.torneo.entity.Team;

public interface TeamService {

    List<TeamDTO> getAllTeams(); // ¿Por qué no te sugiere un getAll o dónde debe sugerirlo?

    TeamDTO saveTeam(Optional<Long> id, TeamDTO teamDTO);

    TeamDTO findByName(String name);

    TeamDTO getTeamById(Long id);
    
    void addPoints(Long idLocal, Long idVisitor, int localPoints, int visitorPoints);
    
    List<TeamDTO> get2Highest();
    
    //List<TeamDTO> get2GeneralTable();

	List<TeamDTO> get2GeneralTable();
}
