package mx.torneo.service;
import java.time.LocalDate;
import java.util.Date;

import mx.torneo.dto.TeamDTO;
import mx.torneo.entity.Color;
import mx.torneo.dto.PlayerDTO;
import mx.torneo.dto.GameDTO;

public interface ValidateService {
    
    void validatePlayer(final PlayerDTO gamerDTO);
    
    void validateTeam(final TeamDTO teamDTO);
    
    void validateGame(final GameDTO gameDTO); //Preguntar por los final
    
    void validatePlayerDate(final LocalDate date, final String error);
    
    /*void validatePlayerName(final String name, final String lastName, final String middleName);
    
    void validatePlayerLastName(final String lastName);
    
    void validateMiddleName(String middleName);*/ 
    
    void validateX(final String name, final String error);
    
    void validateNumber(final int number, final String error);
    
    void validateTeamName(final String name);
    
    void validateTeamLocalColor(final Color localColor);
    
    void validateTeamVisitorColor(final Color visitorColor);
    
    void validateTeams(final boolean isGreater, final String error);

}
