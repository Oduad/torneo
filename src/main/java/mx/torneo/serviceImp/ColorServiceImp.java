package mx.torneo.serviceImp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.torneo.dto.ColorDTO;
import mx.torneo.dto.PlayerDTO;
import mx.torneo.dto.TeamDTO;
import mx.torneo.entity.Color;
import mx.torneo.entity.Player;
import mx.torneo.mapper.Mapper;
import mx.torneo.repository.ColorRepository;
import mx.torneo.service.ColorService;

@Service
public class ColorServiceImp implements ColorService {

	private ColorRepository colorRepository;
	private Mapper mapper;
	
	@Autowired		//Con esto decimos que alguien más crea el colorRepository
	public ColorServiceImp(ColorRepository colorRepository, Mapper mapper) {
		this.colorRepository = colorRepository;
		this.mapper = mapper;
	}
	  
	@Override
	public List<ColorDTO> getAllColors() {
		List<ColorDTO> colorsResponse = new ArrayList<>();
        for (Color color : colorRepository.findAll()) {
            colorsResponse.add(mapper.color2colorDTO(color));
        }
        return colorsResponse;
	}
}
