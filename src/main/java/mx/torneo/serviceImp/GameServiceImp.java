package mx.torneo.serviceImp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.torneo.dto.GameDTO;
import mx.torneo.dto.TeamDTO;
import mx.torneo.entity.Team;
import mx.torneo.exception.DuplicateTeamException;
import mx.torneo.exception.IncompleteTeamException;
import mx.torneo.exception.IncompleteTeamsException;
import mx.torneo.exception.InvalidDateException;
import mx.torneo.exception.InvalidHourException;
import mx.torneo.exception.NotFoundException;
import mx.torneo.entity.Game;
import mx.torneo.mapper.Mapper;
import mx.torneo.repository.GameRepository;
import mx.torneo.repository.TeamRepository;
import mx.torneo.service.GameService;

@Service
public class GameServiceImp implements GameService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TeamServiceImp.class);

	private GameRepository gameRepository;
	private TeamRepository teamRepository;
	private Mapper mapper;
	private TeamServiceImp team;

	@Autowired
	public GameServiceImp(GameRepository gameRepository, Mapper mapper, TeamRepository teamRepository, TeamServiceImp team) {
		this.gameRepository = gameRepository;
		this.teamRepository = teamRepository;
		this.team = team;
		this.mapper = mapper;
	}

	@Override
	public List<GameDTO> getAllGames() {
		List<GameDTO> gamesListDTO = new ArrayList<GameDTO>();
		List<Game> gameList = gameRepository.findAll();
		GameDTO gameDTO = new GameDTO();
		for (int i = 0; i < gameList.size(); i++) {
			gameDTO = mapper.game2gameDTO(gameList.get(i));
			gamesListDTO.add(gameDTO);
		}
		return gamesListDTO;
	}

	@Override
	public boolean validateAllTeams() {
		boolean isGreater = false;
		List<Team> teamList = teamRepository.findAll();
		TeamDTO teamDTO = new TeamDTO();
		for (int i = 0; i < teamList.size(); i++) {
			teamDTO = mapper.team2teamDTO(teamList.get(i));
			if (teamDTO.getPlayers().size() > 2) {
				isGreater = true;
				System.out.println(isGreater);
				System.out.println(teamList.get(i).getName() + " has " + teamDTO.getPlayers().size() + " players.");
			} else {
				isGreater = false;
				System.out.println(isGreater);
				System.out.println(teamList.get(i).getName() + " has " + teamDTO.getPlayers().size() + " players.");
				throw new IncompleteTeamsException("team.incomplete", null);
			}
		}
		return isGreater;
	}

	@Override
	public List<GameDTO> generateAllGames() {
		List<GameDTO> gamesListDTO = new ArrayList<GameDTO>();
		List<Team> teams = teamRepository.findAll();
		Game game = new Game();
		if (teams.size() == 6 && validateAllTeams() == true) {
			for (int i = 0; i < teams.size(); i++) {
				for (int j = 1 + i; j < teams.size(); j++) {
					if (i != j) {
						GameDTO gameDTO = new GameDTO();
						//gameDTO.setDate(teams.get(i).getDate());
						gameDTO.setIdLocal(teams.get(i).getId());
						gameDTO.setLocalTeam(teams.get(i).getName());
						gameDTO.setIdVisitor(teams.get(j).getId());
						gameDTO.setVisitorTeam(teams.get(j).getName());
						game = mapper.gameDTO2game(gameDTO);
						game = gameRepository.save(game);
						gameDTO.setId(game.getId());
						gamesListDTO.add(gameDTO);
					}
				}
			}
			return gamesListDTO;
		}
		throw new IncompleteTeamsException("teams.incomplete", null);
	}

	@Override
	public GameDTO fillResult(Long id, GameDTO gameDTO) {
		Long idLocal, idVisitante;
		Integer puntosLocal, puntosVisitante, contador=0;
		String date;
		String[] horas = { "18:00-18:15", "18:30-18:45", "19:00-19:15" };

		Optional<Game> game = gameRepository.findById(id); // Buscar en la BD
		if (game.isPresent()) { // Si encontramos el id en la BD
			if (game.get().getId().equals(gameDTO.getId())) {
				LOGGER.error("This game was already registered.");
				throw new DuplicateTeamException("game.played", null);
			} else {
				game.get().setLocalPoints(gameDTO.getLocalPoints()); // Aquí tengo que poner el de JSON
				game.get().setVisitorPoints(gameDTO.getVisitorPoints()); // Aquí tengo que poner el de JSON
				game.get().setDate(gameDTO.getDate());
				idLocal = game.get().getIdLocal();
				idVisitante = game.get().getIdVisitor();
				puntosLocal = gameDTO.getLocalPoints();
				puntosVisitante = gameDTO.getVisitorPoints();
				date = gameDTO.getDate();
				for (int i = 0; i < horas.length; i++) {
					System.out.println(gameDTO.getTime() + " y " + horas[i]);
					if (gameDTO.getTime().equals(horas[i])) {
						game.get().setTime(gameDTO.getTime());
						System.out.println("La hora insertada es " + gameDTO.getTime());
					}
					if (gameDTO.getTime().equals(horas[i]) == false) {
						contador++;
					}
					if (contador == 3) {
						throw new InvalidDateException("team.dateInv", null);
					}
				}
			}
			System.out.println("J S ON local entity : " + gameDTO.getIdLocal());
			System.out.println("J S ON Id visitante : " + gameDTO.getIdVisitor());
			System.out.println("J S ON Local points : " + gameDTO.getLocalPoints());
			System.out.println("JSON Visitor points : " + gameDTO.getVisitorPoints());
			System.out.println("La fecha JSON Fecha : " + gameDTO.getDate());
			team.addPoints(idLocal, idVisitante, puntosLocal, puntosVisitante);
			gameRepository.save(game.get());
			return mapper.game2gameDTO(game.get());
		}
		throw new NotFoundException("not.found", null);
	}
}
