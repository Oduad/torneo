package mx.torneo.serviceImp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import mx.torneo.dto.PlayerDTO;
import mx.torneo.dto.TeamDTO;
import mx.torneo.entity.Player;
import mx.torneo.entity.Team;
import mx.torneo.exception.IncompleteTeamException;
import mx.torneo.exception.NotFoundException;
import mx.torneo.mapper.Mapper;
import mx.torneo.repository.PlayerRepository;
import mx.torneo.repository.TeamRepository;
import mx.torneo.service.PlayerService;

@Service
public class PlayerServiceImp implements PlayerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(TeamServiceImp.class);
	private PlayerRepository playerRepository;
	private TeamRepository teamRepository;
	private Mapper mapper;

	@Autowired
	public PlayerServiceImp(PlayerRepository playerRepository, TeamRepository teamRepository, Mapper mapper) {
		this.playerRepository = playerRepository;
		this.teamRepository = teamRepository;
		this.mapper = mapper;
	}

	@Override
	public List<PlayerDTO> getAllPlayers() {
		List<PlayerDTO> playersResponse = new ArrayList<>();
		for (Player player : playerRepository.findAll()) {
			playersResponse.add(mapper.player2playerDTO(player));
		}
		return playersResponse;
	}

	private Player getPlayer(final Long idPlayer) {
		Optional<Player> team = playerRepository.findById(idPlayer);
		if (team.isPresent()) {
			return team.get();
		}
		// throw new BadRequestException("product.not.found", new Object[]{idTeam});
		return null;
	}

	@Override
	public PlayerDTO getPlayerById(final Long idPlayer) {
		Player player = getPlayer(idPlayer);
		return mapper.player2playerDTO(player);
	}

	@Override
	public PlayerDTO savePlayer(final Optional<Long> idTeam, final PlayerDTO playerDTO) {
		// List<Player> playersList = new ArrayList<Player>();
		Player player;
		System.out.println(idTeam);
		System.out.println("El jugador tiene este id: " + playerDTO.getTeam().getId());
		Optional<Team> team = teamRepository.findById(playerDTO.getTeam().getId());
		
		if (team.isEmpty()) {
			throw new NotFoundException("not.found", null);
		} else {
			if (idTeam.isPresent()) { 						// IT IS AN UPDATE
				player = getPlayer(idTeam.get());
				System.out.println("Imprimamos al jugador: "+player);
				player = mapper.playerDTO2player(playerDTO);
			} else { 										// IT IS A SAVE
				if (team.get().getPlayers().size() <= 5) {
					System.out.println("El tamaño del equipo es de "+team.get().getPlayers().size());
					player = mapper.playerDTO2player(playerDTO);
					player = playerRepository.save(player);
					team.get().getPlayers().add(player);
					teamRepository.save(team.get());
					System.out.println("Aquí: "+team.get().getPlayers().add(player));
					System.out.println("Jugador de nombre "+player.getName());
				} else {
					throw new IncompleteTeamException("team.full", null);
				}
			}
			return mapper.player2playerDTO(player);
		}
	}
	// ListaManyToOne tiene que ser Entidad

	private Team getTeam(final Long idTeam) {
		Optional<Team> team = teamRepository.findById(idTeam);
		if (team.isPresent()) {
			return team.get();
		}
		throw new NotFoundException("team.nonexistent", new Object[] { idTeam });
	}

	@Override
	public TeamDTO getTeamById(final Long idTeam) {
		Team team = getTeam(idTeam);
		return mapper.team2teamDTO(team);
	}

	@Override
	public List<PlayerDTO> getPlayersByTeam(final Optional<Long> idTeam) {
		return null;
	}

	/*
	 * @Override public List<PlayerDTO> getPlayersByTeam(final Optional<Integer>
	 * idTeam) { List<PlayerDTO> playersList = new ArrayList<PlayerDTO>();
	 * Optional<Team> team = teamRepository.findById(idTeam); if (team.isEmpty()) {
	 * throw new NotFoundException("not.found", null); }else { if
	 * (idTeam.isPresent()) { // It's an update
	 * 
	 * } return mapper.player2playerDTO(player); } return null; }
	 */
}
