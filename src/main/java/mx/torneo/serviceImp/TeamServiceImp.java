package mx.torneo.serviceImp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import mx.torneo.dto.TeamDTO;
import mx.torneo.entity.Game;
import mx.torneo.entity.Team;
import mx.torneo.exception.DuplicateTeamException;
import mx.torneo.exception.NotFoundException;
import mx.torneo.exception.TeamIsFullyException;
import mx.torneo.mapper.Mapper;
import mx.torneo.repository.GameRepository;
import mx.torneo.repository.TeamRepository;
import mx.torneo.service.TeamService;

@Service
public class TeamServiceImp implements TeamService {
	private static final Logger LOGGER = LoggerFactory.getLogger(TeamServiceImp.class);
	private TeamRepository teamRepository;
	private GameRepository gameRepository;
	private Mapper mapper;

	@Autowired
	public TeamServiceImp(TeamRepository teamRepo, Mapper mapper, GameRepository gameRepository) {
		this.teamRepository = teamRepo;
		this.mapper = mapper;
		this.gameRepository = gameRepository;
	}

	@Override
	public List<TeamDTO> getAllTeams() {
		List<TeamDTO> teamsResponse = new ArrayList<>();
		for (Team team : teamRepository.findAll()) {
			teamsResponse.add(mapper.team2teamDTO(team));
		}
		return teamsResponse;
	}

	@Override
	public TeamDTO saveTeam(final Optional<Long> idTeam, final TeamDTO teamDTO) {
		Team team;

		List<Team> teams = teamRepository.findAllByName(teamDTO.getName());
		List<Team> teams2 = teamRepository.findAll();
		List<Team> teams3 = teamRepository.findByName(teamDTO.getName());

		if (idTeam.isPresent()) { // IT IS AN UPDATE
			team = getTeam(idTeam.get());
			System.out.println("Imprimamos al equipo: " + team);
			team = mapper.teamDTO2team(teamDTO);
		}
		// IT IS A SAVE
		else {
			team = mapper.teamDTO2team(teamDTO);
			if (teams2.size() == 0) {
				System.out.println("Existen un equipo.");
				teams2.add(team);
				team = teamRepository.save(team);
			}
			if (teams2.size() > 0 && teams2.size() < 6) {
				for (int i = 0; i < teams2.size(); i++) {
					if (teams2.get(i).getName().equals(teamDTO.getName())) {
						System.out.println("Ese equipo ya existe!");
						throw new DuplicateTeamException("team.duplicate", null);
					}
				}
				System.out.println("Existen " + (teams2.size() + 1) + " equipos.");
				teams.add(team);
				team = teamRepository.save(team);
				System.out.println(idTeam);
				System.out.println("El equipo tiene este id: " + team.getId());
			}

			if (teams2.size() == 6) {
				System.out.println("El torneo ya tiene 6 equipos!");
				throw new TeamIsFullyException("tournament.full", null);
			}
		}
		return mapper.team2teamDTO(team);

	}

	@Override
	public TeamDTO findByName(String name) {
		List<Team> teams = teamRepository.findAll();
		TeamDTO teamsResponse = new TeamDTO();
		for (int i = 0; i < teams.size(); i++) {
			if (teams.get(i).getName().equals(name)) {
				teamsResponse = mapper.team2teamDTO(teams.get(i));
				return teamsResponse;
			}
		}
		throw new NotFoundException("team.nonexistent", new Object[] { name });

	}

	private Team getTeam(final Long idTeam) {
		Optional<Team> team = teamRepository.findById(idTeam);
		if (team.isPresent()) {
			return team.get();
		}
		throw new NotFoundException("team.nonexistent", new Object[] { idTeam });
	}

	@Override
	public TeamDTO getTeamById(final Long idTeam) {
		Team team = getTeam(idTeam);
		return mapper.team2teamDTO(team);
	}

	@Override
	public void addPoints(Long idLocal, Long idVisitor, int localPoints, int visitorPoints) {
		int lPoints = 0;
		int vPoints = 0;

		Optional<Team> team1 = teamRepository.findById(idLocal);
		Optional<Team> team2 = teamRepository.findById(idVisitor);

		if (localPoints > visitorPoints) {
			lPoints = 3 + team1.get().getPoints();
			team1.get().setPoints(lPoints);
		} else if (localPoints == visitorPoints) {
			lPoints = 1 + team1.get().getPoints();
			vPoints = 1 + +team2.get().getPoints();
			team1.get().setPoints(lPoints);
			team2.get().setPoints(vPoints);
		} else {
			vPoints = 3 + team1.get().getPoints();
			team2.get().setPoints(vPoints);
		}
	}

	/*
	 * @Override public List<TeamDTO> get2GeneralTable() { List<Team> teamsList =
	 * teamRepository.findAll(); List<TeamDTO> teamsListDTO = new
	 * ArrayList<TeamDTO>(); TeamDTO teamDTO; String date; List<Game> teami =
	 * gameRepository.findByDate(null);
	 * 
	 * if (teamsList.size()>2) { System.out.println(); }
	 * 
	 * //game.gameCounter(); for (Team team : teamsList) { teamDTO =
	 * mapper.team2teamDTO(team); teamsListDTO.add(teamDTO); } return teamsListDTO;
	 * }
	 */

	@Override
	public List<TeamDTO> get2GeneralTable() {
		List<Team> teamsList = teamRepository.findAll();
		List<TeamDTO> teamsListDTO = new ArrayList<TeamDTO>();
		List<Game> gamesList = gameRepository.findAll();
		List<Game> gamesListApp = new ArrayList<Game>();
		TeamDTO teamDTO;
		for (int i = 0; i < gamesList.size(); i++) {
			if (gamesList.get(i).getDate() != null) {
				gamesListApp.add(gamesList.get(i));
			}
		}
		if (gamesListApp.size() % 3 == 0) {
			System.out.println(gamesListApp.size());
			System.out.println("Ya se jugaron todos los partidos del día :)");
			for (int i = 0; i < teamsList.size(); i++) {
				teamDTO = mapper.team2teamDTOforTable(teamsList.get(i));
				teamsListDTO.add(teamDTO);
			}
			Collections.sort(teamsListDTO, Collections.reverseOrder());
		} else {
			System.out.println("No se han jugado todos los partidos del día :(");
			System.out.println(gamesListApp.size());
			System.out.println("Como se han jugado " + gamesListApp.size() + " partidos, solo se mostrarán "
					+ (teamsList.size() - teamsList.size() % 3) + " partidos.");
			for (int i = 0; i < teamsList.size() - teamsList.size() % 3; i++) {

			}
			Collections.sort(teamsListDTO, Collections.reverseOrder());
		}
		return teamsListDTO;
	}

	@Override
	public List<TeamDTO> get2Highest() {
		List<Team> teamsList = teamRepository.findAll();
		List<TeamDTO> teamsListDTO = new ArrayList<TeamDTO>();
		TeamDTO teamDTO;

		for (int i = 0; i < teamsList.size(); i++) {
			teamDTO = mapper.team2teamDTO(teamsList.get(i));
			teamsListDTO.add(teamDTO);
		}
		Collections.sort(teamsListDTO, Collections.reverseOrder());
		return teamsListDTO.subList(0, 2);
	}

}
