package mx.torneo.serviceImp;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import mx.torneo.dto.TeamDTO;
import mx.torneo.entity.Color;
import mx.torneo.exception.IncompleteTeamsException;
import mx.torneo.exception.InvalidDateException;
import mx.torneo.exception.InvalidNumberException;
import mx.torneo.exception.StringNullException;
import mx.torneo.dto.PlayerDTO;
import mx.torneo.dto.GameDTO;
import mx.torneo.service.ValidateService;

@Service
public class ValidateServiceImp implements ValidateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ValidateServiceImp.class);

	@Override
	public void validatePlayerDate(final LocalDate date, final String error) {
		// Obtenemos la fecha actual
		LocalDate hoy = LocalDate.now();
		// Obtenemos la fecha hace 10 años
		LocalDate tenYearsAgo = hoy.minusYears(10);
		// Comparamos si la fecha ingresada es posterior de la fecha hace 10 años
		if (date.isBefore(tenYearsAgo)) {
			//System.out.println(date + " está antes que " + tenYearsAgo);
			throw new InvalidDateException(error, null);
		} else {
			System.out.println(date + " está antes que " + tenYearsAgo +" y tiene que ser un jugador menor de 10 años.");
		}
	}

	/*
	 * @Override public void validatePlayerName(final String name) { if (name ==
	 * null) { throw new NameNullException("name.null", null); } }
	 * 
	 * @Override public void validatePlayerLastName(final String lastName) { if
	 * (lastName == null) { throw new NameNullException("lastname.null", null); } }
	 * 
	 * @Override public void validateMiddleName(final String middleName) { if
	 * (middleName == null) { throw new NameNullException("middlename.null", null);
	 * } }
	 */

	/*
	 * Compresión public void validatePlayerName(final String name, final String
	 * lastName, final String middleName) { if (name == null) { throw new
	 * NameNullException("name.null", null); } if (lastName == null) { throw new
	 * NameNullException("lastname.null", null); } if (middleName == null) { throw
	 * new NameNullException("middlename.null", null); } }
	 */

	// Compresión final:
	@Override
	public void validateX(final String string, final String error) {
		if (string == null) {
			throw new StringNullException(error, null);
		}
	}

	@Override
	public void validateNumber(final int number, final String error) {
		if (number <= 0) {
			throw new InvalidNumberException(error, null);
		}
	}

	@Override
	public void validatePlayer(PlayerDTO gamerDTO) {
		/*if(gamerDTO.getName()==) {
			//¿Aquí compara con todos?
		}*/

	}

	@Override
	public void validateTeam(TeamDTO teamDTO) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validateGame(GameDTO gameDTO) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validateTeamName(String name) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validateTeamLocalColor(Color localColor) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validateTeamVisitorColor(Color visitorColor) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validateTeams(boolean isGreater, final String error) {
		if (isGreater == false) {
			throw new IncompleteTeamsException(error, null);
		}
	}
}
